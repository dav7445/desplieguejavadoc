import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

/**
 * Clase para la gestion del articulo con distintos metodos
 * segun la accion que haya escogido el usuario
 * 
 * @author Davidvi
 * @version 1.2
 * @date 1/3/2022
 */
public class GestionArticulo {
	ArrayList<Articulo> articulos;

	/**
	 * Crea un objecto Arraylist
	 */
	public GestionArticulo() {
		articulos = new ArrayList<Articulo>();
	}

	/**
	 * Incluye varios articulos predeterminados
	 */
	public void rellenar() {
		articulos.add(new Articulo("222", "Libro", 30.5, 40.0, 50));
		articulos.add(new Articulo("555", "Revista", 5.5, 7.5, 10));
		articulos.add(new Articulo("111", "Boligrafo", 15.5, 17.5, 60));
		articulos.add(new Articulo("666", "Lapiz", 1.5, 2.5, 50));
		articulos.add(new Articulo("333", "Libreta", 11.5, 12.5, 40));
	}

	/**
	 * Muestra todos los articulos por consola
	 */
	public void visualizar() {
		for (Articulo arti : articulos) {
			System.out.println(arti.toString());
			System.out.println();
		}
		System.out.println();
	}

	/**
	 * Busca un articulo por codigo, recorriendo todos los articulos
	 * 
	 * @param codigo del articulo 
	 * @return true si se encuentra el articulo
	 */
	public boolean buscarcodigo(String codigo) {
		for (Articulo arti : articulos) {
			if (arti.getCodigo().equalsIgnoreCase(codigo)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Metodo para introducir articulos
	 */
	public void introducir() {
		Scanner teclado = new Scanner(System.in);
		boolean cod = false;
		String resp = "si";
		String codigo, descripcion;
		double preciocompra, precioventa;
		int stock;
		System.out.println("Introducir codigo del articulo a dar de alta");
		codigo = teclado.nextLine();
		cod = buscarcodigo(codigo);
		// Mientras el codigo introducido este repetido, volver a preguntar
		while (cod == true) {
			System.out.println("El codigo introducido ya existe");
			System.out.println("Introducir un codigo nuevo");
			codigo = teclado.nextLine();
			cod = buscarcodigo(codigo);
		}
		// Mientras el usuario quiera seguir introduciendo datos, volver a pedirlos
		while (resp.equalsIgnoreCase("si")) {
			System.out.println("Introducir descripcion");
			descripcion = teclado.nextLine();
			System.out.println("Introducir precio de compra");
			preciocompra = teclado.nextDouble();
			System.out.println("Introducir precio de venta");
			precioventa = teclado.nextDouble();
			System.out.println("Introducir stock");
			stock = teclado.nextInt();
			articulos.add(new Articulo(codigo, descripcion, preciocompra, precioventa, stock));
			System.out.println("Quieres introducir mas datos (SI/NO)?");
			resp = teclado.next();
			teclado.nextLine();
		}
	}

	/**
	 * Metodo para dar de baja un articulo segun su codigo
	 */
	public void baja() {
		Scanner teclado = new Scanner(System.in);
		boolean cod = false;
		String codigo;
		System.out.println("Introducir codigo del articulo a dar de baja");
		codigo = teclado.nextLine();
		// Recorre todos los articulos para buscar el articulo por codigo y borrarlo si lo encuentra
		for (int i = 0; i < articulos.size(); i++) {
			if (articulos.get(i).getCodigo().equalsIgnoreCase(codigo)) {
				articulos.remove(i);
				cod = true;
			}
		}
		if (cod == true) {
			System.out.println(codigo + " esta en la lista y se borrara");
		}
		if (cod == false) {
			System.out.println("El codigo introducido no esta en la lista de articulos");
		}
		System.out.println();
	}

	/**
	 * Metodo para dar de baja un articulo segun su codigo, usando un Iterator
	 */
	public void eliminar() {
		Scanner teclado = new Scanner(System.in);
		Iterator<Articulo> arti = articulos.iterator();
		Articulo art;
		boolean cod = false;
		String codigo;
		System.out.println("Introducir codigo del articulo a borrar");
		codigo = teclado.nextLine();
		while (arti.hasNext()) {
			art = arti.next();
			if (art.getCodigo().contains(codigo)) {
				cod = true;
				arti.remove();
			}
		}
		if (cod == true) {
			System.out.println("El articulo con el codigo introducido se borrara");
		} else {
			System.out.println("El codigo introducido no existe");
		}
		System.out.println();
	}

	/**
	 * Modifica las propiedades de un articulo segun su codigo
	 */
	public void modificar() {
		Scanner teclado = new Scanner(System.in);
		boolean cod = false;
		int pos = 0;
		String codigo, descripcion;
		double preciocompra, precioventa;
		int stock;
		System.out.print("Introducir codigo del articulo a modificar: ");
		codigo = teclado.nextLine();
		// Recorre todos los articulos
		for (int i = 0; i < articulos.size(); i++) { 
			if (articulos.get(i).getCodigo().equalsIgnoreCase(codigo)) {
				cod = true;
				pos = i;
			}
		}
		if (cod == true) {
			System.out.print("Introducir nueva descripcion: ");
			descripcion = teclado.nextLine();
			System.out.print("Introducir nuevo precio de compra: ");
			preciocompra = teclado.nextDouble();
			System.out.print("Introducir nuevo precio de venta: ");
			precioventa = teclado.nextDouble();
			System.out.print("Introducir nuevo stock: ");
			stock = teclado.nextInt();
			Articulo art = new Articulo(codigo, descripcion, preciocompra, precioventa, stock);
			articulos.set(pos, art);
		}

		else if (cod == false) {
			System.out.println("El codigo introducido no existe");
		}

		System.out.println();
	}

	/**
	 * Metodo que permite aumentar la cantidad disponible de un articulo segun su codigo
	 */
	public void aumentostock() {
		Scanner teclado = new Scanner(System.in);
		String codigo;
		boolean cod = false;
		int pos = 0;
		int entradas, stockactual;
		System.out.print("Introducir codigo del articulo a aumentar stock: ");
		codigo = teclado.nextLine();
		for (int i = 0; i < articulos.size(); i++) {
			if (articulos.get(i).getCodigo().equalsIgnoreCase(codigo)) {
				pos = i;
				cod = true;
			}
		}
		if (cod == true) {
			System.out.println("El codigo introducido es: " + articulos.get(pos).getCodigo());
			System.out.print("Introducir numero de las nuevas unidades del articulo: ");
			entradas = teclado.nextInt();
			System.out.println(
					"El numero de nuevas unidades del articulo " + articulos.get(pos).getCodigo() + " es " + entradas);
			stockactual = entradas + articulos.get(pos).getStock();
			articulos.get(pos).setStock(stockactual);
			System.out.println(
					"El nuevo stock del articulo con codigo " + articulos.get(pos).getCodigo() + " es " + stockactual);
		} else {
			System.out.println("El codigo introducido no existe");
		}
		System.out.println();
	}

	/**
	 * Metodo que permite vender articulos segun su codigo
	 * Algunos articulos tienen oferta
	 */
	public void ventas() {
		Scanner teclado = new Scanner(System.in);
		String codigo;
		String[] ofertas = { "111", "333", "555", "777", "999" };
		boolean cod = false;
		boolean oferta = false;
		double preciofinal = 0;
		int pos = 0;
		int ventas = 0, stockactual;
		double preciototal, subtotal;
		double iva;
		System.out.print("Introducir codigo del articulo a vender: ");
		codigo = teclado.nextLine();

		for (int i = 0; i < articulos.size(); i++) {
			if (articulos.get(i).getCodigo().equalsIgnoreCase(codigo)) {
				pos = i;
				cod = true;
			}
		}
		if (cod == true) {
			System.out.println("El codigo introducido es: " + articulos.get(pos).getCodigo());
			System.out.print("Introducir numero unidades del articulo que se quiere vender: ");
			ventas = teclado.nextInt();
			// Mientras la cantidad a vender sea mayor a la disponible, preguntar la cantidad a vender
			while (ventas > articulos.get(pos).getStock()) {
				System.out.println("El stock disponible es menor que el numero de unidades");
				System.out.print("Introduzca un numero de unidades menor que el stock disponible: ");
				ventas = teclado.nextInt();
			}
			System.out.println(
					"El numero de unidades vendidas del articulo " + articulos.get(pos).getCodigo() + " es " + ventas);
			stockactual = (articulos.get(pos).getStock()) - ventas;
			articulos.get(pos).setStock(stockactual);
			System.out.println(
					"El nuevo stock del articulo con codigo " + articulos.get(pos).getCodigo() + " es " + stockactual);
		} else if (cod == false) {
			System.out.println("El codigo introducido no existe");
		}

		for (int j = 0; j < ofertas.length; j++) {
			if (ofertas[j].equalsIgnoreCase(codigo)) {
				oferta = true;
			}
		}
		/**
		 * Si el codigo introducido se encuentra entre las ofertas, realizar un 10% de descuento
		 */
		if (oferta == true) {
			System.out.println("El articulo tiene un 10% de descuento");
			preciofinal = (articulos.get(pos).getPreciodeventa()) - (articulos.get(pos).getPreciodeventa() * 10 / 100);
			System.out.println("El precio final del articulo es " + preciofinal);
			System.out.println();
		}

		else if (oferta == false) {
			System.out.println("El articulo no tiene descuento");
			preciofinal = articulos.get(pos).getPreciodeventa();
			System.out.println("El precio final del articulo es " + preciofinal);
			System.out.println();
		}
		/**
		 * Mostrar tabla con toda la informacion del producto a vender y aplicar IVA
		 */
		System.out.println();
		subtotal = preciofinal * ventas;
		System.out.println("CODIGO | DESCRIPCION | UNIDADES| PRECIO UNIDAD| SUBTOTAL ");
		System.out.println("---------------------------------------------------------");
		System.out.println(articulos.get(pos).getCodigo() + "   |   " + articulos.get(pos).getDescripcion() + "   |   "
				+ ventas + "   |   " + preciofinal + "      |   " + subtotal);
		System.out.println("Base Imponible = " + subtotal);
		iva = subtotal * 0.21;
		System.out.println("I.V.A. (21%) = " + iva);
		preciototal = subtotal + iva;
		System.out.println("Total = " + preciototal);
		System.out.println();
	}
}
