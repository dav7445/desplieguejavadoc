import java.util.Scanner;

/**
 * Clase que permite el control y venta de articulos en una tienda
 * 
 * @author Davidvi
 * @version 1.0
 * @date 1/3/2022
 */
public class MainArticulo {

	public static void main(String[] args) {
		menu();
	}

	/**
	 * Metodo para generar el menu con las distintas opciones y permitir al usuario introducir un numero por teclado para escoger una
	 */
	public static void menu() {
		Scanner teclado = new Scanner(System.in);
		int op;
		GestionArticulo arti = new GestionArticulo();
		// Rellena arti con articulos predeterminados
		arti.rellenar();
		// Muestra el menu en bucle hasta que el usuario pulse 8
		do {
			System.out.println("1-Listado");
			System.out.println("2-Alta");
			System.out.println("3-Baja");
			System.out.println("4-Modificacion");
			System.out.println("5-Entrada de Mercancia");
			System.out.println("6-Venta");
			System.out.println("7-Eliminar con Iterator");
			System.out.println("8-Salir");
			System.out.print("Introduzca una opcion: ");
			op = teclado.nextInt();

			switch (op) {
				case 1:
					arti.visualizar();
					break;
				case 2:
					arti.introducir();
					break;
				case 3:
					arti.baja();
					break;
				case 4:
					arti.modificar();
					break;
				case 5:
					arti.aumentostock();
					break;
				case 6:
					arti.ventas();
					break;
				case 7:
					arti.eliminar();
					break;
				case 8:
					System.out.println("FIN PROGRAMA");
					break;
			}
		} while (op != 8);
	}
}
