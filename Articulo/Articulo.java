/**
 * Clase del Articulo
 * 
 * @author Davidvi
 * @version 1.2
 * @date 1/3/2022
 */
public class Articulo {
	private String codigo;
	private String descripcion;
	private Double preciodecompra;
	private Double preciodeventa;
	private Integer stock;

	/**
	 * Objeto articulo con sus caracteristicas correspondientes
	 * 
	 * @param codigo - codigo del articulo
	 * @param descripcion - detalles del articulo
	 * @param preciodecompra - precio de compra
	 * @param preciodeventa - precio de venta
	 * @param stock - cantidad
	 */
	public Articulo(String codigo, String descripcion, Double preciodecompra, Double preciodeventa, Integer stock) {
		super();
		this.codigo = codigo;
		this.descripcion = descripcion;
		this.preciodecompra = preciodecompra;
		this.preciodeventa = preciodeventa;
		this.stock = stock;
	}

	/**
	 * Devuelve el codigo del articulo
	 * 
	 * @return codigo del articulo
	 */
	public String getCodigo() {
		return codigo;
	}

	/**
	 * Establece el codigo del articulo
	 * 
	 * @param codigo del articulo
	 */
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	/**
	 * Devuelve la descripcion del articulo
	 * 
	 * @return descripcion del articulo
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * Establece la descripcion del articulo
	 * 
	 * @param descripcion del articulo
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * Devuelve el precio de compra del articulo
	 * 
	 * @return precio de compra del articulo
	 */
	public Double getPreciodecompra() {
		return preciodecompra;
	}

	/**
	 * Establece el precio de compra del articulo
	 * 
	 * @param precio de compra del articulo
	 */
	public void setPreciodecompra(Double preciodecompra) {
		this.preciodecompra = preciodecompra;
	}

	/**
	 * Devuelve el precio de venta del articulo
	 * 
	 * @return precio de venta del articulo
	 */
	public Double getPreciodeventa() {
		return preciodeventa;
	}

	/**
	 * Establece el precio de venta del articulo
	 * 
	 * @param precio de venta del articulo
	 */
	public void setPreciodeventa(Double preciodeventa) {
		this.preciodeventa = preciodeventa;
	}

	/**
	 * Devuelve el stock del articulo
	 * 
	 * @return cantidad del articulo
	 */
	public Integer getStock() {
		return stock;
	}

	/**
	 * Establece el stock del articulo
	 * 
	 * @param cantidad del articulo
	 */
	public void setStock(Integer stock) {
		this.stock = stock;
	}

	@Override
	public String toString() {
		return "Articulo [codigo=" + codigo + ", descripcion=" + descripcion + ", preciodecompra=" + preciodecompra
				+ "EUR, preciodeventa=" + preciodeventa + "EUR, stock=" + stock + "]";
	}
}
